# README #

Welcome to the repository for code.je.

### What is this repository for? ###

At Codentia, we believe that Open Source is important - and that by publishing some (in reality quite a lot) of our code, we ultimately stand to benefit. To that end, this repository contains the code for our own website as a demonstration that this is something you can do (Open Source because... why not?) - and also as it might be a useful example to someone trying to do something similar.

Our site is integrated with Cloudeware CMS and also serves as an example of how to achieve this.

### Licence ###

This code is provided under the MIT Licence - see http://opensource.org/licenses/MIT for details. Essentially you can do what you want with it, as long as you provide attribution back to us and don't hold us liable if it breaks, or eats your Guinea Pigs.

### Contribution guidelines ###

If you've got a suggestion or would like to submit a pull request, please do. We're trying to be as open minded as possible about this process and improvements are always welcome. Of course, this is the code for our actual real live website, so please don't be offended if we choose not to accept your submission!

### Who do I talk to? ###

For more information, please visit [code.je](http://code.je/) or contact [hello@code.je](mailto:hello@code.je).