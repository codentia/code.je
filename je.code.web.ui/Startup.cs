﻿namespace je.code.web.ui
{
    using je.code.web.ui.Models;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using System.Linq;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.Net.Http.Headers;

    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: false)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", true, false)
            .AddEnvironmentVariables();

            // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<AppSettings>();
            }

            this.Configuration = builder.Build();
            this.Environment = env;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddOptions();

/*            services.AddLocalization(options =>
                    options.ResourcesPath = "Resources"
                );
*/
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            /*            services.AddCors(c =>
                        {
                            c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
                        });*/
            
            services.AddWebOptimizer(pipeline =>
                {
                    pipeline.AddCssBundle("/css/core.min.css", "assets/css/core.css").UseContentRoot();
                    pipeline.AddJavaScriptBundle("/js/scripts.min.js", "assets/js/scripts.js").UseContentRoot();
                }
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment() || env.IsStaging() || env.EnvironmentName == "QA")
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Register middleware
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + durationInSeconds;
                }
            });
            
            app.UseWebOptimizer();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Content}/{action=Index}/{id?}");
            });
        }
    }
}