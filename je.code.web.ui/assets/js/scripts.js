jQuery.noConflict();
(function ($) {

		$(document).ready(function () {
			"use strict";

			$('html').addClass("js").removeClass("no-js");

			// Mobile Menu Trigger

			$('.menu-trigger').on( 'click' , function (e) {
				e.preventDefault();

				$('.header').toggleClass('menu-active');


			});

			// Hover events for touch devices

			$('.header').bind('touchstart touchend', function() {

					$(this).toggleClass('hover');
			});

			// Back to top
			if ( $("#btt").length > 0 ) {
					//Check to see if the window is top if not then display button
					$(window).scroll(function(){
						if ($(this).scrollTop() > 100) {
							$('#btt').fadeIn(300);
						} else {
							$('#btt').fadeOut(300);
						}
					});

					//Click event to scroll to top
					$('#btt').on( 'click' , function(e){
						e.preventDefault();
						$('html, body').animate({scrollTop : 0},800);
					});

				}


				// Polyfill SVG
				if ( ltie8 ) {

					$('img[src$=".svg"]').each(function(index,element) {
							element.src = element.src.replace('.svg','.png');
					});
				}


		    // scroll back to form location if necessary
			if ($('.scroll-position').length > 0)
			{
				$(document).scrollTop($(".scroll-position").offset().top - 100);
			}

		}); // end doc ready



})(jQuery);
