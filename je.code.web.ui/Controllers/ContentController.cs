﻿namespace je.code.web.ui.Controllers
{
    using je.code.web.ui.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using je.code.web.ui.Components;

    public class ContentController : BaseController
    {
        public ContentController(IOptions<AppSettings> appSettings)
        : base(appSettings)
        {
        }

        [Route("")]
        public async Task<IActionResult> Index()
        {
            await Task.Delay(0);
            return View();
        }

        [Route("about")]
        public async Task<IActionResult> Team()
        {
            await Task.Delay(0);
            return View();
        }

        [Route("services")]
        public async Task<IActionResult> Development()
        {
            await Task.Delay(0);
            return View();
        }

        [Route("training")]
        public async Task<IActionResult> Training()
        {
            await Task.Delay(0);
            return View();
        }

        [Route("terms-conditions-privacy")]
        public async Task<IActionResult> Terms()
        {
            await Task.Delay(0);
            return View();
        }
        
        [Route("sitemap.xml")]
        [Produces("application/xml")]
        [ResponseCache(Duration = 3600)]
        public IActionResult Sitemap()
        {
            // Create new XML document and define namespace
            string     host = Request.Host.Host;
            XDocument  doc  = new XDocument();
            XNamespace ns   = "http://www.sitemaps.org/schemas/sitemap/0.9";
            
            // Add all URLs as elements in the XML file
            doc.Add(
                new XElement(ns + "urlset",
                    new XElement(ns + "url",
                        new XElement(ns + "loc", Url.ActionLink("Index"))),
                    new XElement(ns + "url",
                        new XElement(ns + "loc", Url.ActionLink("Team"))),
                    new XElement(ns + "url",
                        new XElement(ns + "loc", Url.ActionLink("Development"))),
                    new XElement(ns + "url",
                        new XElement(ns + "loc", Url.ActionLink("Training"))),
                    new XElement(ns + "url",
                        new XElement(ns + "loc", Url.ActionLink("Terms")))
                ));
            
            // Need to save the document to generate an XML declaration
            StringBuilder xmlStringBuilder = new StringBuilder();
            doc.Save(new StringWriter(xmlStringBuilder));
            
            // Return file as a content result
            return new ContentResult
            {
                Content = xmlStringBuilder.ToString(), 
                ContentType = "application/xml", 
                StatusCode = 200 
            };
        }
    }
}
