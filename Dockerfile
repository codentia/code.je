﻿FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIG
WORKDIR /app

# copy everything and build app
COPY *.sln .
COPY je.code.web.ui.lib/. ./je.code.web.ui.lib/
COPY je.code.web.ui/. ./je.code.web.ui/
RUN dotnet restore

WORKDIR /app/je.code.web.ui
RUN dotnet publish -c $BUILD_CONFIG -o out

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app
COPY --from=build /app/je.code.web.ui/out ./
ENTRYPOINT ["dotnet", "je.code.web.ui.dll"]