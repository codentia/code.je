﻿namespace je.code.web.ui.Components
{
    using je.code.web.ui.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class BaseController : Controller
    {
        public BaseController(IOptions<AppSettings> appSettings)
        {
            AppSettings.Current = appSettings.Value;
        }
    }
}