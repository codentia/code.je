﻿using System;
namespace je.code.web.ui
{
    public class AppSettings
    {
        public static AppSettings Current { get; set; }

        public bool ShowAnalytics { get; set; }
    }
}
